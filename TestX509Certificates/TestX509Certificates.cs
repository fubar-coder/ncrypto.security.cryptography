/***********************************************************************************
'
' This program is free software; you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation; either version 2 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' Copyright (C) 2004 Hernan de Lahitte (http://weblogs.asp.net/hernandl)
'
'***********************************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Security.Cryptography;

using Microsoft.Web.Services2.Security.X509;
using NCrypto.Security.Cryptography;

namespace TestX509Certificates
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class TestX509 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ComboBox cbStore;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txOutput;
		private System.Windows.Forms.TextBox txInput;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button bClose;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox cbStoreLocation;
		private System.Windows.Forms.ComboBox cbCertificate;
		private System.Windows.Forms.Button bExecute;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox cbMethod;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public TestX509()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.bExecute = new System.Windows.Forms.Button();
			this.cbStore = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.cbCertificate = new System.Windows.Forms.ComboBox();
			this.txOutput = new System.Windows.Forms.TextBox();
			this.txInput = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.bClose = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.cbStoreLocation = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.cbMethod = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// bExecute
			// 
			this.bExecute.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bExecute.Location = new System.Drawing.Point(232, 329);
			this.bExecute.Name = "bExecute";
			this.bExecute.TabIndex = 0;
			this.bExecute.Text = "Go";
			this.bExecute.Click += new System.EventHandler(this.bExecute_Click);
			// 
			// cbStore
			// 
			this.cbStore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStore.Location = new System.Drawing.Point(67, 13);
			this.cbStore.Name = "cbStore";
			this.cbStore.Size = new System.Drawing.Size(128, 21);
			this.cbStore.TabIndex = 1;
			this.cbStore.SelectedIndexChanged += new System.EventHandler(this.cbStore_OnSelIndexChange);
			// 
			// label1
			// 
			this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label1.Location = new System.Drawing.Point(12, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 23);
			this.label1.TabIndex = 2;
			this.label1.Text = "Store:";
			// 
			// label2
			// 
			this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label2.Location = new System.Drawing.Point(12, 50);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(52, 23);
			this.label2.TabIndex = 4;
			this.label2.Text = "Certificate:";
			// 
			// cbCertificate
			// 
			this.cbCertificate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbCertificate.Location = new System.Drawing.Point(67, 46);
			this.cbCertificate.Name = "cbCertificate";
			this.cbCertificate.Size = new System.Drawing.Size(320, 21);
			this.cbCertificate.TabIndex = 3;
			// 
			// txOutput
			// 
			this.txOutput.Location = new System.Drawing.Point(67, 144);
			this.txOutput.Multiline = true;
			this.txOutput.Name = "txOutput";
			this.txOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txOutput.Size = new System.Drawing.Size(320, 172);
			this.txOutput.TabIndex = 5;
			this.txOutput.Text = "";
			// 
			// txInput
			// 
			this.txInput.Location = new System.Drawing.Point(67, 112);
			this.txInput.Name = "txInput";
			this.txInput.Size = new System.Drawing.Size(320, 20);
			this.txInput.TabIndex = 6;
			this.txInput.Text = "This is a sample text to be signed.";
			// 
			// label3
			// 
			this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label3.Location = new System.Drawing.Point(12, 115);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 23);
			this.label3.TabIndex = 7;
			this.label3.Text = "Input:";
			// 
			// label4
			// 
			this.label4.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label4.Location = new System.Drawing.Point(12, 145);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(52, 23);
			this.label4.TabIndex = 8;
			this.label4.Text = "Output:";
			// 
			// bClose
			// 
			this.bClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.bClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bClose.Location = new System.Drawing.Point(312, 329);
			this.bClose.Name = "bClose";
			this.bClose.TabIndex = 9;
			this.bClose.Text = "Close";
			this.bClose.Click += new System.EventHandler(this.bClose_Click);
			// 
			// label5
			// 
			this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label5.Location = new System.Drawing.Point(213, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(48, 23);
			this.label5.TabIndex = 11;
			this.label5.Text = "Location:";
			// 
			// cbStoreLocation
			// 
			this.cbStoreLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStoreLocation.Location = new System.Drawing.Point(267, 13);
			this.cbStoreLocation.Name = "cbStoreLocation";
			this.cbStoreLocation.Size = new System.Drawing.Size(120, 21);
			this.cbStoreLocation.TabIndex = 10;
			this.cbStoreLocation.SelectedIndexChanged += new System.EventHandler(this.cbStoreLocation_OnSelIndexChange);
			// 
			// label6
			// 
			this.label6.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label6.Location = new System.Drawing.Point(12, 84);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(52, 23);
			this.label6.TabIndex = 13;
			this.label6.Text = "Method:";
			// 
			// cbMethod
			// 
			this.cbMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbMethod.Items.AddRange(new object[] {
														  "Sign",
														  "VerifySignature"});
			this.cbMethod.Location = new System.Drawing.Point(67, 80);
			this.cbMethod.Name = "cbMethod";
			this.cbMethod.Size = new System.Drawing.Size(320, 21);
			this.cbMethod.TabIndex = 12;
			// 
			// TestX509
			// 
			this.AcceptButton = this.bExecute;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.bClose;
			this.ClientSize = new System.Drawing.Size(402, 364);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.cbMethod);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.cbStoreLocation);
			this.Controls.Add(this.bClose);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txInput);
			this.Controls.Add(this.txOutput);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.cbCertificate);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cbStore);
			this.Controls.Add(this.bExecute);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "TestX509";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Test X509 Certicates";
			this.Load += new System.EventHandler(this.TestX509_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.EnableVisualStyles();
			Application.Run(new TestX509());
		}

		private void TestX509_Load(object sender, System.EventArgs e)
		{
			cbStore.Items.Add( X509CertificateStore.MyStore );
			cbStore.Items.Add( X509CertificateStore.RootStore );
			cbStore.Items.Add( X509CertificateStore.CAStore );
			cbStore.Items.Add( X509CertificateStore.OtherPeople );
			cbStore.Items.Add( X509CertificateStore.TrustStore );
			cbStore.Items.Add( X509CertificateStore.UnTrustedStore );

			cbStoreLocation.Items.Add( X509CertificateStore.StoreLocation.CurrentUser.ToString() );
			cbStoreLocation.Items.Add( X509CertificateStore.StoreLocation.LocalMachine.ToString() );

			cbStore.SelectedIndex = 0;
			cbMethod.SelectedIndex = 0;
		}

		private void bClose_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void cbStore_OnSelIndexChange(object sender, System.EventArgs e)
		{
			cbStoreLocation.SelectedIndex = 0;
		}

		private void cbStoreLocation_OnSelIndexChange(object sender, System.EventArgs e)
		{
			LoadCertificates();
			cbCertificate.SelectedIndex = 0;
		}

		private void bExecute_Click(object sender, System.EventArgs e)
		{
			string result = String.Empty;

			switch( cbMethod.SelectedItem.ToString() )
			{
				case "Sign":
					result = Sign( txInput.Text );
					break;
				case "VerifySignature":
					result = VerifySignature( txInput.Text, txOutput.Text ).ToString();
					break;
				case "Encrypt":
					//TODO
					break;
				case "Decrypt":
					//TODO
					break;
			}
			txOutput.Text = result;
		}

		#region "Execute Methods"

		private string Sign( string input )
		{
			string result = String.Empty;

			//Open the certificate store
			using( X509CertificateStore store = new X509CertificateStore( 
					   X509CertificateStore.StoreProvider.System, 
					   (X509CertificateStore.StoreLocation)Enum.Parse( typeof(X509CertificateStore.StoreLocation), cbStoreLocation.SelectedItem.ToString()), 
					   cbStore.SelectedItem.ToString() ))
			{
				if( store.OpenRead() )
				{
					//Fetch the selected certificate
					X509CertificateCollection certs = store.FindCertificateBySubjectName( cbCertificate.SelectedItem.ToString() );

					if( certs.Count > 0 )
					{
						// For WSE 1.x version
						//result = CryptoHelper.Sign( input, (AsymmetricAlgorithm)certs[0].Key );
						// For WSE 2.0 version (Asym Formatters are custom and not from System.Security.Cryptography
                        Microsoft.Web.Services2.Security.Cryptography.RSACryptoServiceProvider rsa = (Microsoft.Web.Services2.Security.Cryptography.RSACryptoServiceProvider)certs[0].Key;
						byte[] hash = CryptoHelper.ComputeHash( Utility.DefaultEncoding.GetBytes( input ), new SHA1Managed() );
						byte[] signature = rsa.SignHash( hash, CryptoConfig.MapNameToOID( "SHA1" ) ); 
						result = Convert.ToBase64String( signature );
					}
				}
			}

			return result;
		}

		private bool VerifySignature( string value, string signature )
		{
			bool result = false;

			//Open the certificate store
			using( X509CertificateStore store = new X509CertificateStore( 
					   X509CertificateStore.StoreProvider.System, 
					   (X509CertificateStore.StoreLocation)Enum.Parse( typeof(X509CertificateStore.StoreLocation), cbStoreLocation.SelectedItem.ToString()), 
					   cbStore.SelectedItem.ToString() ))
			{
				if( store.OpenRead() )
				{
					//Fetch the selected certificate
					X509CertificateCollection certs = store.FindCertificateBySubjectName( cbCertificate.SelectedItem.ToString() );

					if( certs.Count > 0 )
					{
						//result = CryptoHelper.VerifySignature( value, signature, (AsymmetricAlgorithm)certs[0].PublicKey );
						// For WSE 2.0 version (Asym Formatters are custom and not from System.Security.Cryptography
						Microsoft.Web.Services2.Security.Cryptography.RSACryptoServiceProvider rsa = (Microsoft.Web.Services2.Security.Cryptography.RSACryptoServiceProvider)certs[0].Key;
						byte[] hash = CryptoHelper.ComputeHash( Utility.DefaultEncoding.GetBytes( value ), new SHA1Managed() );
						result = rsa.VerifyHash( hash, CryptoConfig.MapNameToOID( "SHA1" ), Convert.FromBase64String( signature ) ); 
					}
				}
			}

			return result;
		}
		#endregion

		#region "Private methods"

		private void LoadCertificates()
		{
			cbCertificate.Items.Clear();

			//Open the certificate store
			using( X509CertificateStore store = new X509CertificateStore( 
					   X509CertificateStore.StoreProvider.System, 
					   (X509CertificateStore.StoreLocation)Enum.Parse( typeof(X509CertificateStore.StoreLocation), cbStoreLocation.SelectedItem.ToString()), 
					   cbStore.SelectedItem.ToString() ))
			{
				if( store.OpenRead() )
				{
					foreach( X509Certificate cert in store.Certificates )
					{
						//Get all certitificates CNs that supports signing.
						if( cert.SupportsDigitalSignature )
						{
							cbCertificate.Items.Add( cert.GetName() );
						}			
					}
				}
			}
		}

		#endregion
	}
}
