using System;
using System.Runtime.InteropServices;

using NCrypto.Security.Cryptography;

namespace TestSecureString
{
	/// <summary>
	/// Summary description for Start.
	/// </summary>
	class Start
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			#region Test Methods
			SecureString ss = new SecureString();
			ss.AppendChar('A');
			ss.AppendChar('b');
			ss.AppendChar('C');
			ss.AppendChar('d');
			ss.InsertAt(1,'Z');
			ss.RemoveAt(2);
			ss.SetAt( 3, 'L');
			IntPtr ptr = SecureString.SecureStringToGlobalAllocUni( ss );
			string tx = Marshal.PtrToStringUni( ptr );
			SecureString.ZeroFreeGlobalAllocUni( ptr );
			#endregion
            
			using( SecureCredential credentials = UICredentialsHelper.PromptForSecureCredentials( "SecureCredentials", "Enter your credentials" ) )
			{
				if( credentials == null )
				{
					return;
				}

				// Make some use of these credentials.
				// ...


				// For debugging purposes, we may trasfer the content inside
				// the SecureString to a ptr and from there to a managed string.
				// In v2.0 you will use the Marshal class instead of the static
				// method SecureString.SecureStringToGlobalAllocUni and
				// SecureString.ZeroFreeGlobalAllocUni as well.
				
				IntPtr ustr = SecureString.SecureStringToGlobalAllocUni( credentials.Password );

				try
				{
					// WARNING: This managed string will remain in memory until
					// collected by the GC.
					string clearTextPwd = Marshal.PtrToStringUni( ustr );
					Console.WriteLine( "Pwd: {0}", clearTextPwd );
				}
				finally
				{
					// Once you're done using the unprotected string, you need 
					// to make sure to erase that copy.
					SecureString.ZeroFreeGlobalAllocUni( ustr );
				}
				
			}
		}
	}
}
